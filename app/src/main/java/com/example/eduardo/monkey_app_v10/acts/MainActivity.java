package com.example.eduardo.monkey_app_v10.acts;

import android.app.Activity;
import android.content.Intent;
import android.nfc.cardemulation.CardEmulation;
import android.os.Bundle;

import com.example.eduardo.monkey_app_v10.cardemulation.PersistenceClass;
import com.example.eduardo.monkey_app_v10.data.AccountStorage;

import java.security.NoSuchAlgorithmException;

/**
 * Created by Eduardo on 13/01/2016.
 */
public class MainActivity extends Activity{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent;
        String account;

        account = AccountStorage.GetAccount(this);

        if(account == null){
            intent = new Intent(this, LoginActivity.class);
        }else{
            intent = new Intent(this, KeyActivity.class);
            String concat;
            String digestString = null;
            concat = AccountStorage.GetAccount(this);

            try {
                digestString = PersistenceClass.makeHash(concat);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }

            PersistenceClass.persistenceHash(this, digestString);
        }

        startActivity(intent);
        finish();
    }



}
