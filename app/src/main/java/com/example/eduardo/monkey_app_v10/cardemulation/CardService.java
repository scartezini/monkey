package com.example.eduardo.monkey_app_v10.cardemulation;

import android.annotation.TargetApi;
import android.content.ComponentName;
import android.nfc.NfcAdapter;
import android.nfc.cardemulation.CardEmulation;
import android.nfc.cardemulation.HostApduService;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import com.example.eduardo.monkey_app_v10.acts.KeyActivity;
import com.example.eduardo.monkey_app_v10.acts.MainActivity;

import java.util.Arrays;

import de.greenrobot.event.EventBus;

import static java.util.Arrays.copyOfRange;

@TargetApi(Build.VERSION_CODES.KITKAT)
public class
CardService extends HostApduService {

    private static final String TAG = "CardService";

    // AID for our loyalty card service.
    private static final String SAMPLE_LOYALTY_CARD_AID_FIST = "F0616B756E74737500";
    private static final String SAMPLE_LOYALTY_CARD_AID_SECOND = "F0616B756E74737501";
    private static final String SAMPLE_LOYALTY_CARD_AID_ERRO = "F0616B756E74737510";
    private static final String SAMPLE_LOYALTY_CARD_AID_OPEN = "F0616B756E74737511";



    // ISO-DEP command HEADER for selecting an AID.
    // Format: [Class | Instruction | Parameter 1 | Parameter 2]
    private static final String SELECT_APDU_HEADER = "00A40400";

    // "OK" status word sent in response to SELECT AID command (0x9000)
    private static final byte[] SELECT_OK_SW = HexStringToByteArray("9000");

    // "UNKNOWN" status word sent in response to invalid APDU command (0x0000)
    private static final byte[] UNKNOWN_CMD_SW = HexStringToByteArray("0000");
    private static final byte[] UNKNOWN_OPEN_RESPONSE = HexStringToByteArray("1111");



    @Override
    public byte[] processCommandApdu(byte[] commandApdu, Bundle extras) {

        String data;
        if (Arrays.equals(BuildSelectApdu(SAMPLE_LOYALTY_CARD_AID_FIST), commandApdu) ) {
            Log.d("Recive fist",ByteArrayToHexString(commandApdu));
            data = PersistenceClass.GetHash(this);
            byte[] dataBytes = HexStringToByteArray(data);


            Log.d("Data length", String.valueOf(data.length()));
            Log.d("DataBytes", ByteArrayToHexString(dataBytes));
            Log.d("DataByte length", ByteArrayToHexString(dataBytes).length()  + "");
            return dataBytes;


        }else if(Arrays.equals(BuildSelectApdu(SAMPLE_LOYALTY_CARD_AID_ERRO), commandApdu)){
            EventBus.getDefault().post(false);

            Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(800);



            return UNKNOWN_OPEN_RESPONSE;
        }else if(Arrays.equals(BuildSelectApdu(SAMPLE_LOYALTY_CARD_AID_OPEN), commandApdu)){
            EventBus.getDefault().post(true);
            Vibrator vibrator = (Vibrator) getApplicationContext().getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(400);

            return UNKNOWN_OPEN_RESPONSE;
        }
        else {
            Log.d("Fist",ByteArrayToHexString(BuildSelectApdu(SAMPLE_LOYALTY_CARD_AID_FIST)));
            Log.d("Second",ByteArrayToHexString(BuildSelectApdu(SAMPLE_LOYALTY_CARD_AID_SECOND)));
            Log.d("Recive",ByteArrayToHexString(commandApdu));

            Log.d("CardService","ERRO");
            return UNKNOWN_CMD_SW;
        }

    }


    @Override
    public void onDeactivated(int reason) {
        Log.d(TAG+" Desativado", reason+"");
    }


    /**
     * Utility method to convert a hexadecimal string to a byte string.
     *
     * <p>Behavior with input strings containing non-hexadecimal characters is undefined.
     *
     * @param s String containing hexadecimal characters to convert
     * @return Byte array generated from input
     * @throws java.lang.IllegalArgumentException if input length is incorrect
     */
    public static byte[] HexStringToByteArray(String s) throws IllegalArgumentException {
        int len = s.length();
        if (len % 2 == 1) {
            throw new IllegalArgumentException("Hex string must have even number of characters");
        }
        byte[] data = new byte[len / 2]; // Allocate 1 byte per 2 hex characters
        for (int i = 0; i < len; i += 2) {
            // Convert each character into a integer (base-16), then bit-shift into place
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }


    /**
     * Build APDU for SELECT AID command. This command indicates which service a reader is
     * interested in communicating with. See ISO 7816-4.
     *
     * @param aid Application ID (AID) to select
     * @return APDU for SELECT AID command
     */
    public static byte[] BuildSelectApdu(String aid) {
        // Format: [CLASS | INSTRUCTION | PARAMETER 1 | PARAMETER 2 | LENGTH | DATA]
        return HexStringToByteArray(SELECT_APDU_HEADER + String.format("%02X",
                aid.length() / 2) + aid);
    }


    /**
     * Utility method to convert a byte array to a hexadecimal string.
     *
     * @param bytes Bytes to convert
     * @return String, containing hexadecimal representation.
     */
    public static String ByteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
        char[] hexChars = new char[bytes.length * 2]; // Each byte has two hex characters (nibbles)
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF; // Cast bytes[j] to int, treating as unsigned value
            hexChars[j * 2] = hexArray[v >>> 4]; // Select hex character from upper nibble
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // Select hex character from lower nibble
        }
        return new String(hexChars);
    }

    /**
     * Utility method to concatenate two byte arrays.
     * @param first First array
     * @param rest Any remaining arrays
     * @return Concatenated copy of input arrays
     */
    public static byte[] ConcatArrays(byte[] first, byte[]... rest) {
        int totalLength = first.length;
        for (byte[] array : rest) {
            totalLength += array.length;
        }
        byte[] result = Arrays.copyOf(first, totalLength);
        int offset = first.length;
        for (byte[] array : rest) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }
        return result;
    }
}


