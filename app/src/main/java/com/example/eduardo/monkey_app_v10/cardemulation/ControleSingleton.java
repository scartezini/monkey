package com.example.eduardo.monkey_app_v10.cardemulation;

/**
 * Created by Eduardo on 12/01/2016.
 */
public class ControleSingleton {
    private static ControleSingleton INSTANCE;
    private Boolean aberto;

    private ControleSingleton() {
        aberto = false;
    }

    public static ControleSingleton getINSTANCE() {
        if(INSTANCE == null){
            INSTANCE = new ControleSingleton();
        }
        return INSTANCE;
    }

    public Boolean getAberto() {
        return aberto;
    }

    public void setAberto(Boolean aberto) {
        this.aberto = aberto;
    }
}
