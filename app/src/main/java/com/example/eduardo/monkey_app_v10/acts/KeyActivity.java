package com.example.eduardo.monkey_app_v10.acts;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.cardemulation.CardEmulation;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;


import com.example.eduardo.monkey_app_v10.R;
import com.example.eduardo.monkey_app_v10.data.AccountStorage;
import com.sprylab.android.widget.TextureVideoView;

import android.os.Handler;

import java.io.File;
import java.io.IOException;
import java.util.logging.FileHandler;

import de.greenrobot.event.EventBus;

/**
 * Created by Eduardo on 11/01/2016.
 */
public class KeyActivity extends Activity {

    private TextureVideoView mAnimator;
    private TextView mTvAbrir;
    private ImageView mIvCenter;
    private ImageView mIvClosed;

    private ImageView mIvOpen;
    private TextView mTvOpen;
    private TextView mTvOpen_below;

    private AnimatorSet animatorSet;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.key_activity);

        mIvOpen = (ImageView) findViewById(R.id.ivOpen);
        mTvOpen = (TextView) findViewById(R.id.tvOpen);
        mTvOpen_below = (TextView) findViewById(R.id.tvOpen2);

        mTvAbrir = (TextView) findViewById(R.id.textView);

        mIvCenter = (ImageView) findViewById(R.id.ivLogo);

        Typeface font = Typeface.createFromAsset(getAssets(), "font/SourceSansPro-Black.ttf");

        mTvOpen.setTypeface(font);
        mTvOpen_below.setTypeface(font);
        mTvAbrir.setTypeface(font);

        mIvClosed = (ImageView) findViewById(R.id.ivClosed);

        mIvClosed.setImageAlpha(0);
        mIvClosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(KeyActivity.this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Exit")
                        .setMessage("Are you sure you want to exit?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                AccountStorage.SetAccount(KeyActivity.this, null);
                                KeyActivity.super.onBackPressed();
                            }
                        }).setNegativeButton("No", null).show();
            }
        });

        EventBus.getDefault().register(KeyActivity.this);


        mTvAbrir.setAlpha(0);




        mIvCenter.setImageAlpha(0);
        mIvCenter.setVisibility(View.VISIBLE);

        ObjectAnimator closedAnimator = ObjectAnimator.ofInt(mIvClosed, "alpha", 0, 255);
        closedAnimator.setStartDelay(100);
        closedAnimator.setDuration(1000);

        ObjectAnimator imageAnimator = ObjectAnimator.ofInt(mIvCenter, "alpha", 0, 255);
        imageAnimator.setStartDelay(200);
        imageAnimator.setDuration(1000);


        ObjectAnimator textAnimator = ObjectAnimator.ofFloat(mTvAbrir, "alpha", 0, 1);
        textAnimator.setStartDelay(400);
        textAnimator.setDuration(1500);


        animatorSet = new AnimatorSet();
        animatorSet.setInterpolator(new AccelerateInterpolator(2));
        animatorSet.play(closedAnimator).with(textAnimator).with(imageAnimator);


        RotateAnimation rotate = new RotateAnimation(
                0,
                440,
                Animation.RELATIVE_TO_SELF,
                0.5f,
                Animation.RELATIVE_TO_SELF,
                0.5f);

        rotate.setDuration(2500);
        mIvClosed.setAnimation(rotate);


        animatorSet.start();

//        mAnimator = (TextureVideoView) findViewById(R.id.viewAnimator);
//
//        try {
//            mAnimator.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.untitled));
//        }catch (Exception e){
//            Log.e("Erro",e.getMessage());
//        }
//        mAnimator.requestFocus();
//        mAnimator.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mp) {
//                View frame = (View) findViewById(R.id.frame_background);
//
//                frame.setVisibility(View.GONE);
//            }
//        });
//
//
//        mAnimator.start();
//        mAnimator.setVisibility(View.VISIBLE);
    }



    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onResume() {
        super.onResume();


        CardEmulation cardEmulation = CardEmulation.getInstance(NfcAdapter.getDefaultAdapter(this));
        ComponentName cp = new ComponentName("com.example.eduardo.monkey_app_v10.cardemulation","CardService");

        cardEmulation.setPreferredService(this, cp);

        Boolean bo = cardEmulation.categoryAllowsForegroundPreference(CardEmulation.CATEGORY_OTHER);
        Log.d("KeyActivity", bo + "");

        int selection = cardEmulation.getSelectionModeForCategory(CardEmulation.CATEGORY_OTHER);
        Log.d("KeyActivity", selection + "");
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    public void onEventMainThread(Boolean open){
        mTvAbrir.setVisibility(View.GONE);
        mIvCenter.setVisibility(View.GONE);

        mTvOpen.setVisibility(View.VISIBLE);
        if(open){
            mIvOpen.setImageResource(R.drawable.acesso_liberado);
            mIvOpen.setVisibility(View.VISIBLE);

            mTvOpen_below.setText("LIBERADO");
            mTvOpen_below.setVisibility(View.VISIBLE);
        }else{
            mIvOpen.setImageResource(R.drawable.acesso_negado);

            mIvOpen.setVisibility(View.VISIBLE);

            mTvOpen_below.setText("NEGADO");
            mTvOpen_below.setVisibility(View.VISIBLE);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mIvOpen.setVisibility(View.GONE);
                mIvCenter.setVisibility(View.VISIBLE);

                mTvOpen.setVisibility(View.GONE);
                mTvOpen_below.setVisibility(View.GONE);

                mTvAbrir.setVisibility(View.VISIBLE);
            }
        },3000);
    }

}
