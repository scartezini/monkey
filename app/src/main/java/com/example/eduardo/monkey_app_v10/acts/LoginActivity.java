package com.example.eduardo.monkey_app_v10.acts;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.cardemulation.CardEmulation;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.eduardo.monkey_app_v10.R;
import com.example.eduardo.monkey_app_v10.cardemulation.PersistenceClass;
import com.example.eduardo.monkey_app_v10.data.AccountStorage;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Eduardo on 11/01/2016.
 */
public class LoginActivity extends Activity implements View.OnClickListener {

    private static final String TAG = "LoginActivit";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);


        Button btnEntrar = (Button) findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        String concat;
        String digestString = null;

        EditText etLogin = (EditText) findViewById(R.id.et_login);
        EditText etSenha = (EditText) findViewById(R.id.et_senha);
        CheckBox cbGravar = (CheckBox) findViewById(R.id.cbMemorizarSenha);

        String login = etLogin.getText().toString().replaceAll("\\s","");
        String senha = etSenha.getText().toString().replaceAll("\\s","");

        concat = senha.concat(login);
        try {
            digestString = PersistenceClass.makeHash(concat);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        if(cbGravar.isChecked()){
          AccountStorage.SetAccount(this,concat);
        }

        PersistenceClass.persistenceHash(this, digestString);


        Intent intent = new Intent(LoginActivity.this, KeyActivity.class);
        startActivity(intent);
    }





}
