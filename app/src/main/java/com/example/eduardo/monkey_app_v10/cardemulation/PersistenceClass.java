package com.example.eduardo.monkey_app_v10.cardemulation;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Eduardo on 15/01/2016.
 */
public class PersistenceClass {
    private static final String TAG = "HashStorage";
    private static final String PREF_HASH_RESULT = "hash_result";
    private static final String PREF_HASH_RESULT1 = "hash_result1";
    private static final String DEFAULT_HASH_RESULT = "";
    private static String sHash = null;
    private static String sHash1 = null;
    private static final Object sHashLock = new Object();
    private static final Object sHashLock1 = new Object();

    public static void SetHash(Context c, String s) {
        synchronized(sHashLock) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
            prefs.edit().putString(PREF_HASH_RESULT, s).commit();
            sHash = s;
        }
    }

    public static String GetHash(Context c) {
        synchronized (sHashLock) {
            if (sHash == null) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
                String account = prefs.getString(PREF_HASH_RESULT, DEFAULT_HASH_RESULT);
                sHash = account;
            }
            return sHash;
        }
    }

    public static void SetHash1(Context c, String s) {
        synchronized(sHashLock1) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
            prefs.edit().putString(PREF_HASH_RESULT1, s).commit();
            sHash1 = s;
        }
    }

    public static String GetHash1(Context c) {
        synchronized (sHashLock1) {
            if (sHash1 == null) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
                String account = prefs.getString(PREF_HASH_RESULT1, DEFAULT_HASH_RESULT);
                sHash1 = account;
            }
            return sHash1;
        }
    }

    public static String makeHash(String input)throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        md.reset();
        byte[] buffer = input.getBytes();
        md.update(buffer);
        byte[] digest = md.digest();

        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
            hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
        }
        return hexStr;
    }


    public static void persistenceHash(Context context, String digest){
        String s = digest.substring(0, 44);
        String s1 = digest.substring(44);
        PersistenceClass.SetHash(context, s);
        PersistenceClass.SetHash1(context, s1);
        Log.i(TAG, digest);
        Log.i(TAG, s);
        Log.i(TAG, s1);
    }


}
